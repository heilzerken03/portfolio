const express = require('express')
const dotenv = require('dotenv')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const mongoose = require('mongoose')

dotenv.config()

const app = express()
const port = 6003

// MongoDB Connection
mongoose.connect(`mongodb+srv://kenheilzer:${process.env.MONGODB_PASSWORD}@cluster0.wtjmdvw.mongodb.net/Portfolio?retryWrites=true&w=majority`, {
    UseNewUrlParser: true,
    UseUnifiedTopology: true
})

let db = mongoose.connection
db.once('open', () => console.log('Connected to MongoDB!'))

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// ROUTES
app.use('/users', userRoutes)

// ROUTES END
app.listen(process.env.PORT || 6003, () => {
    console.log(`API is now running on localhost: ${process.env.PORT || 6003}`)
})




