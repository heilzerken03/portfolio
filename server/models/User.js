const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Name is required"]
    },
    email: {
        type: String,
        required: [true, "Email Address is required."]
    },
    phoneNumber: {
        type: String,
        required: [true, "Phone Number is required."]
    },
    message: {
        type: String,
        default: true
    },
    InquiredOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('User', user_schema)